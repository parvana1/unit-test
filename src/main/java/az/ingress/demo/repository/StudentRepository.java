package az.ingress.demo.repository;

import az.ingress.demo.model.Student;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Integer> {

//    @Query(value = "select * from student where age = :age", nativeQuery = true)
//    List<Student> findStudentWithAgeEquals(Integer age);

    //JPQL - Java persistence query language

//    @Query("select s from Student s left join fetch s.phoneList")
//    @EntityGraph(value = "students_with_phone_list", type = EntityGraph.EntityGraphType.FETCH)
    List<Student> findAllBy();

    @Override
    @Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
    Optional<Student> findById(Integer integer);

}
