package az.ingress.demo.repository;

import az.ingress.demo.model.Account;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Integer> {

    Optional<Account> findByName(String name);

    @Lock(LockModeType.OPTIMISTIC)
    @Override
    Optional<Account> findById(Integer integer);
}
