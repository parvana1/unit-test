package az.ingress.demo.repository;

import az.ingress.demo.model.Balance;
import az.ingress.demo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BalanceRepository extends JpaRepository<Balance, Integer> {


}
