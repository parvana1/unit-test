package az.ingress.demo.mapper;

import az.ingress.demo.dto.StudentDto;
import az.ingress.demo.model.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface StudentMapper {

    StudentDto entityToDto(Student student);


    Student dtoToEntity(StudentDto student);

    List<StudentDto> listEntityToDto(List<Student> student);
}
