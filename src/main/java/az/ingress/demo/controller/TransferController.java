package az.ingress.demo.controller;

import az.ingress.demo.model.Account;
import az.ingress.demo.repository.AccountRepository;
import az.ingress.demo.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transfer/v1")
@RequiredArgsConstructor
@Slf4j
public class TransferController {

    private final AccountRepository accountRepository;
    private final TransferService transferService;

    @GetMapping("/{id}")
    public Account get(@PathVariable Integer id) {
        return accountRepository.findById(id).get();
    }


    @PutMapping
    @Transactional
    public void transfer() {
        log.info("Getting account with id 1");
        Account from = accountRepository.findById(5).get(); //280
        log.info("FROM account is {}", from);
        log.info("Getting account with id 2");
        Account to = accountRepository.findById(6).get(); //300
        log.info("TO account is {}", to);
        transferService.transfer(from, to, 20);
        log.info("TO account is {}", to);
    }

    @PutMapping("/withdraw/{amount}")
    public void withdraw(@PathVariable Integer amount) {
        log.info("WITHDRAW:Getting account with id 1");
        Account from = accountRepository.findById(5).get(); //
        log.info("WITHDRAW:FROM account is {}", from);
        transferService.withdraw(from, amount);
    }


}
