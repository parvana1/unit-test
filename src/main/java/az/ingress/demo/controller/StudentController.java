package az.ingress.demo.controller;

import az.ingress.demo.dto.StudentDto;
import az.ingress.demo.model.Student;
import az.ingress.demo.service.StudentService;
import org.hibernate.annotations.Cache;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student/v1")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/{id}")
    public StudentDto get(@PathVariable Integer id) {
        return studentService.get(id);
    }


    @GetMapping("/all")
    public List<StudentDto> all() {
        return studentService.findAll();
    }

    @PostMapping
    public StudentDto create(@RequestBody StudentDto student) {

        return studentService.create(student);
    }

    @PutMapping("/{id}")
    public StudentDto update(@PathVariable Integer id, @RequestBody Student student) {
        return studentService.update(id, student);
    }

    @PutMapping("/{id}/sleep")
    public StudentDto update2(@PathVariable Integer id, @RequestBody Student student) {
        return studentService.update2(id, student);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        studentService.delete(id);
    }

}
