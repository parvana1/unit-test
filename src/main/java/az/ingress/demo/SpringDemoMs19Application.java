package az.ingress.demo;

import az.ingress.demo.model.Account;
import az.ingress.demo.model.Student;
import az.ingress.demo.repository.AccountRepository;
import az.ingress.demo.repository.StudentRepository;
import az.ingress.demo.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
@Slf4j
@EnableCaching
public class SpringDemoMs19Application implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(SpringDemoMs19Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        List<Student> a = new ArrayList<>();
//        for (int i = 0; i < 10000; i++) {
//            a.add(Student.builder()
//                            .id(UUID.randomUUID())
//                    .name("Ali " + i)
//                    .lastname("Mammadov " + i)
//                            .age(i)
//                    .build());
//        }
//        studentRepository.saveAll(a);
//        System.out.println("done");
    }

}
