package az.ingress.demo.service;

import az.ingress.demo.dto.StudentDto;
import az.ingress.demo.model.Student;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

public interface StudentService {
    List<StudentDto> findAll();

    StudentDto get(Integer id);

    StudentDto create(StudentDto student);

    StudentDto update(Integer id, Student student);

    void delete(Integer id);

    StudentDto update2(Integer id, Student student);
}
