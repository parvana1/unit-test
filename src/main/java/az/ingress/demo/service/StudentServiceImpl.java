package az.ingress.demo.service;

import az.ingress.demo.dto.StudentDto;
import az.ingress.demo.mapper.StudentMapper;
import az.ingress.demo.model.Student;
import az.ingress.demo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;


    @Override
    public List<StudentDto> findAll() {
        return studentRepository.findAll().stream().map(studentMapper::entityToDto).toList();
    }


    @Override
    public StudentDto get(Integer id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        return studentMapper.entityToDto(student);
    }

    @Override
    public StudentDto create(StudentDto studentDto) {
        log.info("Student service create method is working");
        Student student = studentMapper.dtoToEntity(studentDto);
        studentRepository.save(student);
        return studentMapper.entityToDto(student);
    }

    @Override
    @Transactional
    public StudentDto update(Integer id, Student student) {
        log.info("Student service update method is working");
            Student entity = studentRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Student not found"));
        entity.setAge(student.getAge());
        entity.setName(student.getName());
        entity.setLastname(student.getLastname());
        entity = studentRepository.save(entity);
        return studentMapper.entityToDto(student);
    }

    @Override
    @Transactional
    public StudentDto update2(Integer id, Student student) {
        log.info("Student service update method is working");
        Student entity = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        entity.setAge(student.getAge());
        entity.setName(student.getName());
        entity.setLastname(student.getLastname());
        entity = studentRepository.save(entity);
        return studentMapper.entityToDto(student);
    }

    @Override
    public void delete(Integer id) {
        log.info("Student service delete method is working");
        studentRepository.deleteById(id);
    }
}
