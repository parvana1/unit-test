package az.ingress.demo.service;

import az.ingress.demo.dto.PhoneDto;

public interface PhoneService {
    PhoneDto create(Integer studentId, PhoneDto phoneDto);
}
