package az.ingress.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentDto implements Serializable {

    private static final long serialVersionUID = 5328743342113L;

    Integer id;
    String name;
    String lastname;
    Integer age;
}
