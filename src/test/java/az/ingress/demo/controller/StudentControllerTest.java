package az.ingress.demo.controller;

import az.ingress.demo.dto.StudentDto;
import az.ingress.demo.model.Student;
import az.ingress.demo.service.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StudentController.class)
@RunWith(SpringRunner.class)
class StudentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private StudentService studentService;

    @Test
    public void givenValidIdWhenGetStudentThenSuccess() throws Exception {
        //Arrange
        StudentDto studentDto = StudentDto.builder()
                .id(5)
                .age(15)
                .lastname("Aaa")
                .name("Bbb")
                .build();
        when(studentService.get(any())).thenReturn(studentDto);

        //Act & Assert
        mockMvc.perform(get("/student/v1/5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(5))
                .andExpect(jsonPath("$.age").value(15))
                .andExpect(jsonPath("$.lastname").value("Aaa"))
                .andExpect(jsonPath("$.name").value("Bbb"));
    }

    @Test
    public void givenValidDtoWhenCreateStudentThenSuccess() throws Exception {
        StudentDto studentDto = StudentDto.builder()
                .id(5)
                .age(15)
                .lastname("Aaa")
                .name("Bbb")
                .build();
        when(studentService.create(any())).thenReturn(studentDto);

        //Act & Assert
        mockMvc.perform(post("/student/v1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(studentDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(5))
                .andExpect(jsonPath("$.age").value(15))
                .andExpect(jsonPath("$.lastname").value("Aaa"))
                .andExpect(jsonPath("$.name").value("Bbb"));
    }
    @Test
    public void givenValidIdWhenUpdateStudentThenSuccess() throws Exception {
        StudentDto student = StudentDto.builder()
                .id(5)
                .age(30)
                .lastname("Peri")
                .name("Rashidova")
                .build();
        when(studentService.update(anyInt(),any())).thenReturn(student);

        //Act & Assert
        mockMvc.perform(put("/student/v1/5")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(student)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.age").value(30))
                .andExpect(jsonPath("$.lastname").value("Peri"))
                .andExpect(jsonPath("$.name").value("Rashidova"));
    }

    @Test
    public void givenValidIdWhenDeleteStudentThenSuccess() throws Exception {
        StudentDto student = StudentDto.builder()
                .id(5)
                .age(30)
                .lastname("Peri")
                .name("Rashidova")
                .build();
        doNothing().when(studentService).delete(student.getId());

        //Act & Assert
        mockMvc.perform(delete("/student/v1/5"))
                .andExpect(status().isOk());
    }
}