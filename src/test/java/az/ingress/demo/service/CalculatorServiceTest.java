package az.ingress.demo.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
class CalculatorServiceTest {

    @InjectMocks
    private CalculatorService calculatorService;

    @Test
    public void givenTwoNumbersWhenSumThenSuccess() {
        int sum = calculatorService.sum(5, 10);
        assertThat(sum).isEqualTo(15);
    }

    @Test
    public void givenTwoNumbersWhenDivideThenSuccess() {
        int sum = calculatorService.divide(10, 5);
        assertThat(sum).isEqualTo(2);
    }

    @Test
    public void givenTwoNumbersWhenDivideToZeroThenException() {

        assertThatThrownBy(()-> calculatorService.divide(10, 0))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Can't divide to zero");
    }

}