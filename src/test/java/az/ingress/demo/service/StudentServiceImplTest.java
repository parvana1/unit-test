package az.ingress.demo.service;

import az.ingress.demo.dto.StudentDto;
import az.ingress.demo.mapper.StudentMapper;
import az.ingress.demo.model.Student;
import az.ingress.demo.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StudentServiceImplTest {

    @InjectMocks
    private StudentServiceImpl service;

    @Mock
    private StudentRepository studentRepository;
    @Mock
    private StudentMapper studentMapper;

    @Test
    public void givenValidIdWhenGetStudentThenSuccess() {
        //Arrange
        Student student = Student.builder()
                .id(5)
                .age(15)
                .lastname("Aaa")
                .name("Bbb")
                .build();

        StudentDto studentDto = StudentDto.builder()
                .id(5)
                .age(15)
                .lastname("Aaa")
                .name("Bbb")
                .build();

        when(studentRepository.findById(anyInt())).thenReturn(Optional.of(student));
        when(studentMapper.entityToDto(any())).thenReturn(studentDto);

        //Act
        StudentDto result = service.get(student.getId());

        //Assert
        assertThat(result.getId()).isEqualTo(5);
        assertThat(result.getAge()).isEqualTo(15);
        assertThat(result.getLastname()).isEqualTo("Aaa");
        assertThat(result.getName()).isEqualTo("Bbb");
        verify(studentRepository, times(1)).findById(any());
        verify(studentRepository, times(0)).save(any());
    }


    @Test
    public void givenInvalidIdWhenGetStudentThenNotFound() {
        //Arrange
        when(studentRepository.findById(anyInt())).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(()->service.get(5)).isInstanceOf(RuntimeException.class)
                .hasMessage("Student not found");
    }

    @Test
    public void givenValidDtoWhenCreateStudentWithSuccess() {
        Student student = Student.builder()
                .id(5)
                .age(15)
                .lastname("Aaa")
                .name("Bbb")
                .build();
        //Arrange
        StudentDto studentDto = StudentDto.builder()
                .id(5)
                .age(15)
                .lastname("Aaa")
                .name("Bbb")
                .build();
        when(studentMapper.dtoToEntity(any())).thenReturn(student);
        when(studentRepository.save(any())).thenReturn(student);
        when(studentMapper.entityToDto(any())).thenReturn(studentDto);

        //Act
       StudentDto saveStudentToDb=service.create(studentDto);

       //Assert
        assertThat(saveStudentToDb.getId()).isEqualTo(5);
        assertThat(saveStudentToDb.getAge()).isEqualTo(15);
        assertThat(saveStudentToDb.getLastname()).isEqualTo("Aaa");
        assertThat(saveStudentToDb.getName()).isEqualTo("Bbb");
    }

    @Test
    public void givenValidIdWhenDeleteStudentThenSuccess() {
        //Arrange
        Student student = Student.builder()
                .id(5)
                .age(15)
                .lastname("Aaa")
                .name("Bbb")
                .build();

        //Act
         service.delete(student.getId());

        //Assert

        verify(studentRepository, times(1)).deleteById(5);
    }

    @Test
    public void givenValidDtoWhenUpdateStudentWithSuccess() {
        Student student = Student.builder()
                .id(5)
                .age(30)
                .lastname("Peri")
                .name("Rashidova")
                .build();
        //Arrange
        StudentDto studentDto = StudentDto.builder()
                .id(5)
                .age(30)
                .lastname("Peri")
                .name("Rashidova")
                .build();
        when(studentRepository.findById(anyInt())).thenReturn(Optional.of(student));
        when(studentRepository.save(any())).thenReturn(student);
        when(studentMapper.entityToDto(any())).thenReturn(studentDto);

        //Act
       StudentDto updatedStudent= service.update(student.getId(),student);

        //Assert
        assertThat(updatedStudent.getAge()).isEqualTo(30);
        assertThat(updatedStudent.getLastname()).isEqualTo("Peri");
        assertThat(updatedStudent.getName()).isEqualTo("Rashidova");
    }
    @Test
    public void givenInvalidIdWhenUpdateStudentThenNotFound() {
        //Arrange

        when(studentRepository.findById(anyInt())).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(()->service.get(5)).isInstanceOf(RuntimeException.class)
                .hasMessage("Student not found");
    }
}